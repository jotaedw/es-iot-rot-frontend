import React from 'react';
import { Admin, Resource } from 'react-admin';
// dataProvider and authProvider
import { restClient, authClient } from 'ra-data-feathers';
// feathersClient
import feathersClient from './api';
// Import Resources
import Devices from './components/Devices';
import Users from './components/Users';
import Measures from './components/Measures';
import Actions from './components/Actions';


const restClientOptions = {
  id: '_id',
  usePatch: true,
};

const authOptions = {
  storageKey: 'feathers-jwt',
};

export default () => (
  <Admin
    title="Admin"
    dataProvider={restClient(feathersClient, restClientOptions)}
    authProvider={authClient(feathersClient, authOptions)}
  >
    <Resource options={{ label: 'Dispositivos' }} name="devices" {...Devices} />
    <Resource options={{ label: 'Usuarios' }} name="users" {...Users} />
    <Resource options={{ label: 'Mediciones' }} name="measures" {...Measures} />
    <Resource options={{ label: 'Acciones' }} name="actions" {...Actions} />
  </Admin>
);
