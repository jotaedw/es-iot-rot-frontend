import React from 'react';
import {
  Edit,
  SimpleForm,
  SelectInput,
  ReferenceInput,
  AutocompleteInput,
  NumberInput,
  required,
} from 'react-admin';


const conditionSymbols = [
  { id: '<', name: '<' },
  { id: '<=', name: '<=' },
  { id: '=', name: '=' },
  { id: '>', name: '>' },
  { id: '>=', name: '>=' },
];

const cooldownUnits = [
  { id: 'ms', name: 'Milisegundos' },
  { id: 's', name: 'Segundos' },
  { id: 'm', name: 'Minutos' },
  { id: 'h', name: 'Horas' },
  { id: 'd', name: 'Días' },
];

const deviceOptionText = ({ nickname, mac }) => `${nickname}, ${mac}`;

export default props => (
  <Edit {...props}>
    <SimpleForm>
      <ReferenceInput label="Sensor" source="sensor._id" reference="devices" validate={required()} filter={{ type: 'sensor' || 'both' }}>
        <AutocompleteInput optionText={deviceOptionText} />
      </ReferenceInput>
      <ReferenceInput label="Actuador" source="actuator._id" reference="devices" validate={required()} filter={{ type: 'actuator' || 'both' }}>
        <AutocompleteInput optionText={deviceOptionText} />
      </ReferenceInput>
      <NumberInput source="cooldown.value" validate={required()} />
      <SelectInput source="cooldown.unit" choices={cooldownUnits} validate={required()} />
      <SelectInput source="condition.symbol" choices={conditionSymbols} validate={required()} />
      <NumberInput source="condition.limitValue" validate={required()} />
    </SimpleForm>
  </Edit>
);
