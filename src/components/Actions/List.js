import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  ReferenceField,
  EditButton,
} from 'react-admin';

import { ActionconditionField } from '../CustomFields';


export default props => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="Usuario" source="user._id" reference="users">
        <TextField source="email" />
      </ReferenceField>
      <ReferenceField label="Sensor" source="sensor._id" reference="devices">
        <TextField source="mac" />
      </ReferenceField>
      <ReferenceField label="Actuador" source="actuator._id" reference="devices">
        <TextField source="mac" />
      </ReferenceField>
      <ActionconditionField source="condition" label="Condición" />
      <EditButton />
    </Datagrid>
  </List>

);
