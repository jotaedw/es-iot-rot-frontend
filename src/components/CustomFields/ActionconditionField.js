import React from 'react';
import { Typography } from '@material-ui/core';
import {
  FunctionField,
} from 'react-admin';


const ActionconditionField = (record) => {
  if (!record.condition) {
    return (<div />);
  }
  const { condition } = record;
  return (<Typography>{`${condition.limitValue} ${condition.symbol}`}</Typography>);
};

export default props => (<FunctionField {...props} render={ActionconditionField} />);
