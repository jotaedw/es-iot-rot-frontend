import React from 'react';
import { Chip } from '@material-ui/core';
import {
  FunctionField,
} from 'react-admin';


const DevicetypeField = ({ type }) => {
  switch (type) {
    case 'sensor':
      return (<Chip label="Sensor" />);
    case 'actuator':
      return (<Chip label="Actuador" />);
    case 'both':
      return (
        <>
          <Chip label="Sensor" />
          <Chip label="Actuador" />
        </>
      );
    default:
      break;
  }
  return <div />;
};

export default props => (<FunctionField {...props} render={DevicetypeField} />);
