import React from 'react';
import { Typography } from '@material-ui/core';
import {
  FunctionField,
} from 'react-admin';


const MeasureField = record => (<Typography>{`${record.measure} ${record.unit}`}</Typography>);

export default props => (<FunctionField {...props} render={MeasureField} />);
