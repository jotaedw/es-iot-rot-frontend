import React from 'react';
import { Chip } from '@material-ui/core';
import {
  FunctionField,
} from 'react-admin';


const RoleField = (record) => {
  switch (record){
    case 'ADMIN':
      return (<Chip label="Administrador" />);
    case 'BASIC':
      return (<Chip label="Basico" />);
    default:
      break;
  }
  return <div />;
};

export default props => (<FunctionField {...props} render={RoleField} />);
