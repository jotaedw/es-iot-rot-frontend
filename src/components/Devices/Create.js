import React from 'react';
import {
  Create,
  SimpleForm,
  TextInput,
  ReferenceInput,
  AutocompleteInput,
  SelectInput,
  required,
} from 'react-admin';


const deviceTypeChoices = [
  { id: 'actuator', name: 'Actuador' },
  { id: 'sensor', name: 'Sensor' },
  { id: 'both', name: 'Ambos' },
];

export default props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="MAC Address" source="mac" validate={[required()]} />
      <TextInput label="Alias" source="nickname" />
      <SelectInput label="Tipo" source="type" validate={[required()]} choices={deviceTypeChoices} />
      <ReferenceInput label="Pertenece a" source="user" reference="users">
        <AutocompleteInput optionText="email" />
      </ReferenceInput>
    </SimpleForm>
  </Create>
);
