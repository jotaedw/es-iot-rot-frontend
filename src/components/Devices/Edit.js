import React from 'react';
import {
  Edit,
  SimpleForm,
  TextInput,
  ReferenceInput,
  AutocompleteInput,
  SelectInput,
  required,
} from 'react-admin';

const DeviceTitle = ({ record: { nickname, mac } }) => (<span>{`${nickname}, ${mac}`}</span>);

const deviceTypeChoices = [
  { id: 'actuator', name: 'Actuador' },
  { id: 'sensor', name: 'Sensor' },
  { id: 'both', name: 'Ambos' },
];

export default props => (
  <Edit {...props} title={<DeviceTitle />}>
    <SimpleForm>
      <TextInput label="MAC Address" source="mac" validate={[required()]} />
      <TextInput label="Alias" source="nickname" />
      <SelectInput label="Tipo" source="type" validate={[required()]} choices={deviceTypeChoices} />
      <ReferenceInput label="Pertenece a" source="user._id" reference="users">
        <AutocompleteInput source="user" optionText="email" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);
