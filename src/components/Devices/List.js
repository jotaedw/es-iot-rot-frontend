import React from 'react';
import {
  List,
  Datagrid,
  TextField,
  SelectField,
  ReferenceField,
  EditButton,
} from 'react-admin';

import { DevicetypeField } from '../CustomFields';

const deviceStatusChoices = [
  { value: 'CONNECTED', text: 'Conectado' },
  { value: 'DISCONNECTED', text: 'Desonectado' },
];

export default props => (
  <List {...props}>
    <Datagrid>
      <TextField label="Alias" source="nickname" />
      <DevicetypeField label="Tipo" source="type" />
      <SelectField label="Estado" source="status" choices={deviceStatusChoices} optionValue="value" optionText="text" />
      <ReferenceField label="Usuario" source="user._id" reference="users">
        <TextField source="email" />
      </ReferenceField>
      <EditButton />
    </Datagrid>
  </List>

);
