import React from 'react';
import {
  List,
  Datagrid,
  ReferenceField,
  TextField,
  EditButton,
} from 'react-admin';

import { MeasureField } from '../CustomFields';


export default props => (
  <List {...props}>
    <Datagrid>
      <ReferenceField label="Dispositivo" source="device._id" reference="devices">
        <TextField source="mac" />
      </ReferenceField>
      <MeasureField label="Medición" />
      <EditButton />
    </Datagrid>
  </List>

);
