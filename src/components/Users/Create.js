import React from 'react';
import {
  Create,
  SimpleForm,
  TextInput,
  SelectArrayInput,
  required,
  email,
} from 'react-admin';

const RoleChoices = [
  { id: 'ADMIN', name: 'Administrador' },
  { id: 'BASIC', name: 'Basico' },
];

export default props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput label="email" source="email" validate={[required(), email()]} />
      <SelectArrayInput label="Roles" source="role" validate={required()} choices={RoleChoices} />
    </SimpleForm>
  </Create>
);
