import React from 'react';
import {
  Edit,
  SimpleForm,
  TextInput,
  SelectArrayInput,
  required,
  email,
} from 'react-admin';

const RoleChoices = [
  { id: 'ADMIN', name: 'Administrador' },
  { id: 'BASIC', name: 'Basico' },
];

export default props => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput label="Email" source="email" validate={[required(), email()]} />
      <TextInput label="Contraseña" source="password" validate={required()} type="password" />
      <SelectArrayInput label="Roles" source="role" validate={required()} choices={RoleChoices} />
    </SimpleForm>
  </Edit>
);
