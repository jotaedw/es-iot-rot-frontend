import React from 'react';
import {
  List,
  Datagrid,
  EmailField,
  ArrayField,
  SingleFieldList,
  EditButton,
} from 'react-admin';

import { RoleField } from '../CustomFields';


export default props => (
  <List {...props}>
    <Datagrid>
      <EmailField label="Email" source="email" />
      <ArrayField label="Roles" source="role">
        <SingleFieldList>
          <RoleField />
        </SingleFieldList>
      </ArrayField>
      <EditButton />
    </Datagrid>
  </List>

);
